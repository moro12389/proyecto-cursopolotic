from django.db import models

# Create your models here.
class Categorias(models.Model):
    nombre=models.CharField(max_length=50)
    descripcion=models.CharField(max_length=200)
    def __str__(self):
        return self.nombre

class Productos(models.Model):
    titulo=models.CharField(max_length=50,null=False,blank=false)
    imagen=models.ImageField(upload_to="productos",null=True)
    descripcion=models.CharField(max_length=200,default="sin descripción",null=True)
    precio=models.FloatField(null=True,default=0)
    categoria=models.ForeignKey(Categorias,on_delete=models.CASCADE,related_name="categorias")
    def __str__(self):
        return self.titulo

class Carrito(models.Model):
    usuario=models.ForeignKey(user)
    lista=models.ManyToManyField(user)